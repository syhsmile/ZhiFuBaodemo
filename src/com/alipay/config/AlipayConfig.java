﻿package com.alipay.config;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2018010701683071";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCbpwQQpwvBGGRQns1IF4OywV6iy9A7EYYBNLq32mMVYn3XTOUDbdizBbR8660FtadUhPpVY1IX7GkCIZ6olAkj3fQUodDbDMo9OiwWgXWZzznEhsIUe2f1Z+vOpTDZhRnNTc74p4JU4rBYky/iQERowO9qcDbeOCiEdpxP1rqX06nWEKMIDR7tttG0QftYvK3YFJQhwNLAmtRYfUI1wbr1dzFp51PlZP1o51iN4LiqAticf80wgQTWNi2BByw6pKvWDoZmdPaZD0ffDoUUs8QOEl/wvTQXEspxsYMB7shraIj3cs0a7O9atn1wsgIv012FFT5HEbpKvNyW1lrmyLK1AgMBAAECggEAGG34N1zgfWgEppqmAYCfTPkIj2iJwBuZ+qp5+80s6/hPY/FPqGAfOMnWcBzVRSYjoyJT3/78rYCPYqRAMJk7bgOyAWL8UsUJIN72eHnNKVza5jt4Tpqn0F6FJiPLLPi0+gIST/iKfiQuJVjElTAdDIvrh7shAXOy+EdddTJwis+QrbPrLgO4CWK2wM5iyrVYF8iJXScl9CUdYlsQyuUfDWSOBB8THuzhLs3vsWYyxUU4g7nDvp/7qFzvJqDHF+F5wa2tz/JI/WeFITeL10XfPl6UHXvbO/6k93ZYlNUMLDMlryVe74beg21e4qty7U2BBBovtluqeLVSXWEnKulCKQKBgQDgHSsPlWjn9+zzp+LZqzKE/ZbpOPrwaE/LtYEjA5/HlDITlYW4cfKDsWCwm9nSGJpeeGo450ogveTFLZiRMbOVhefAA4ebNJvmYikPPr/YKq+WjoKte7A1y+hKwiNVwPYLAgzHv6BGGILls9RDF4ZQwwBVXkTwXIaUdcltghvtNwKBgQCxzEtyP6Xn40DHnHIn+OXT6CnqSEf43IW7WNtKZxjLDFx58uq11RvDJRsTrZZNXpYtXLkO4CSqcgAVwfy5YdQG2os8g+BxDYdHMAZWDtq4wwkglyRCBNCDReJOUhJnb2EsN+xdib7loUb/a8WqK43AT7civGo1/FqKwyJqx0V1cwKBgB8v7+4bPJ4LQk4l7subp0wafgW2iDUsTu5qM65LstBzLwTKqfEg6j6pUGtDROyXv+OlniKNDSPBSTSnQ7Pw1qzYFYgC9bwDdi55On2Ene3XAthQvJ6nt73Ewm6k1YX5dEPT5TNm10bp4k1U2RT9ERPsDv/SPoW0WoYOXhl/IaPBAoGAWNth5VI75v0jFd9xCdsReWsw7356QbxsNWo+BhwqPugJm4S0mnVL76YutxHs6PPZmAgcEHwitvBMbJNLa5ahsV8tcOvzkmyRbBlrQT0hQG9ws1H18+yWDNUzObM4sWrvpHq6bTD3L6n/Yo2I9vfXWwI742dIqXxZ/7lwDO9UHlMCgYEAm2gY3xp/HraOCcVcdPqFbbZUiDL4O7OoqGbDV8TlAc+/oiqyqKGxOda4oThTspfi8m8v05w4TwLvkAe1mI4JTKuLHNd+XrZnorvC9KVCaAHkv0Iz6cgfTYiBsIGqzYUJ0Pjq0jvtPRObz1J+bbtmoYfS9+WBBT6Bp6/80lXqr5c=";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm6cEEKcLwRhkUJ7NSBeDssFeosvQOxGGATS6t9pjFWJ910zlA23YswW0fOutBbWnVIT6VWNSF+xpAiGeqJQJI930FKHQ2wzKPTosFoF1mc85xIbCFHtn9WfrzqUw2YUZzU3O+KeCVOKwWJMv4kBEaMDvanA23jgohHacT9a6l9Op1hCjCA0e7bbRtEH7WLyt2BSUIcDSwJrUWH1CNcG69XcxaedT5WT9aOdYjeC4qgLYnH/NMIEE1jYtgQcsOqSr1g6GZnT2mQ9H3w6FFLPEDhJf8L00FxLKcbGDAe7Ia2iI93LNGuzvWrZ9cLICL9NdhRU+RxG6SrzcltZa5siytQIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://localhost:8082/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://localhost:8082/alipay.trade.page.pay-JAVA-UTF-8/return_url.jsp";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipay.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "F:\\SoftDev\\ALiPay\\log";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

